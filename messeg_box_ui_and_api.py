from PyQt5.QtWidgets import QMainWindow, QMessageBox
from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap
import queue as Queue

class Result_Box_UI ():
	def showDonePopUp (self):
		""" Shows a success message box """
		msg_box = QMessageBox (self)
		msg_box.setText ("Success in merging file!")
		msg_box.setWindowTitle ("Update")
		msg_box.setStandardButtons(QMessageBox.Discard)
		msg_box.setDefaultButton(QMessageBox.Discard)
		#msg_box.setIconPixmap(QPixmap("./ui_graphic/done.png"))
		temp = msg_box.exec_ ()

	def showErrorPopUp (self):
		""" Shows a Error message box """
		msg_box = QMessageBox ()
		msg_box.setWindowTitle ("Update")
		msg_box.setText ("Failed to merge file!")
		#msg_box.setIconPixmap(QPixmap("./ui_graphic/done.png"))
		temp = msg_box.exec_ ()