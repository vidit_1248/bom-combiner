from bom_combiner_routine import *

class MainGUICallbacks ():
	def __init__(self, gui_obj, bom_comb):
		self.gui_obj = gui_obj

		# Define set callback for add file button
		self.gui_obj.ui.addfile_but.clicked.connect (self.addFileCb)
		self.gui_obj.ui.outputbrowse_but.clicked.connect (self.outBrowseCb)
		self.gui_obj.ui.mergebom_but.clicked.connect (self.mergeBOMCb)

		# Instance of BOM Combiner Class
		self.combiner = bom_comb
		
		# Browse button related things
		self.file_count = 0

	def addFileCb (self):
		self.gui_obj.addFileEntry (self.file_count)
		self.gui_obj.perFileObjests [self.file_count]['browse_but'].clicked.connect (self.browseBOMFileCb)
		self.file_count += 1

	def browseBOMFileCb (self):
		source_button = self.gui_obj.ui.scrollarea_widget.sender ()
		but_name = source_button.objectName ()
		rcv_path = self.gui_obj.openFileDialogAndGetFilePath ()
		if rcv_path:
			file_index = but_name [11:]
			self.gui_obj.updateFilePathTesxtEdit (int(file_index), rcv_path[0])

	def outBrowseCb (self):
		self.gui_obj.outputFileSavePathAndUpdateGUI ()
		
	def mergeBOMCb (self):
		'''	1. Disable the Marge button and update it's name to proccesing 
			2. Collect all the paths and qtys
			3. Send all these to merge BOM code : Using queue method 
			4. Give popup after the thing is done or fails : BOM Combiner will call GUI API for this
			5. Then Change back the merge BOM back to normal : BOM Combiner will call GUI API for this '''
		# Step 1
		self.gui_obj.setMergeButStateTo (0)
		self.gui_obj.disableAndchangeNameOfMergBut ("Proccesing")
		# Step 2 and 3
		try:
			self.combiner.combine_q.put ([self.gui_obj.getOutputPath (), self.gui_obj.getAllPathAndQty ()])
		except:
			e = sys.exc_info()[0]
			print ("Failed in collecting information from GUI: ", e)