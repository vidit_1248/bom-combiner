from os import error
from main_win_gui_api import *
import queue as Queue
import openpyxl
import sys
from shutil import copyfile

class BOM_Combiner (QtCore.QThread):
	def __init__ (self, gui_obj):
		QtCore.QThread.__init__ (self)
		self.thread_active = False
		self.combine_q = Queue.Queue()
		self.gui_obj = gui_obj

	def run (self):
		self.thread_active = True
		while  self.thread_active:
			if not self.combine_q.empty ():
				value_list = self.combine_q.get ()
				try:
					self.combineFiles (value_list [0], value_list [1])
				except:
					e = sys.exc_info()[0]
					print ("Cimbine failed in combine file thread: ", e)
				self.gui_obj.enableAndchangeNameOfMergBut ("Merge all BOM")
				print ("Combine Suncess")

	def exit (self):
		self.thread_active = False

	def getQtyValPackFrom (self, sheet_obj, row_num):
		part_data = {
			"value" : sheet_obj.cell (row = row_num, column = 2).value,
			"pack" : sheet_obj.cell (row = row_num, column = 4).value,
			"qty" : sheet_obj.cell (row = row_num, column = 1).value
		}
		return part_data

	def multiplyAllQty (self, sheet_obj, multiplier, wb_obj):
		for row_loop_count in range (2, sheet_obj.max_row + 1):
			new_qty = sheet_obj.cell (row = row_loop_count, column = 1).value * multiplier
			sheet_obj.cell (row = row_loop_count, column = 1).value = new_qty
		wb_obj.save (self.bom_out_path)

	def combineFiles (self, out_path, src_list):
		# Transfer the path data to self variable of class
		self.bom_out_path = out_path
		# Step 1: Copy the Sheet 1 from the list as the output file 
		copyfile (src_list[0][0], self.bom_out_path)
		# Prepare variables and open the output BOM Sheet
		self.wb_out = openpyxl.load_workbook (self.bom_out_path)
		self.sheet_out = self.wb_out.active
		# Step 2: Update the qauntity of the sheet one acording to multipler given by user
		if src_list[0][1] != 1:
			print ("Updating the qauntity of parts copied from sheet one into output file")
			self.multiplyAllQty (self.sheet_out, src_list[0][1], self.wb_out)
		else:
			print ("User gave 1 as multipler, no need to update")

		# Step 3: Take one file at a time and copy parts from it into the output file
		#			It will check if part aleardy exist or not. If part already exist, 
		# 			it will update the qauntity according to multiplier. The code compares
		# 			the part using it's value and footprint, because that's what really matters
		# This loop goes through the list of source files and thier qauntity
		for src_data in src_list [1:]:
			# Get path and qauntity of next source sheet 
			src_path = src_data [0]
			src_qty = src_data [1]

			# Reset count of row and column of output file at start of every new sheet
			row_count_out = self.sheet_out.max_row
			col_count_out = self.sheet_out.max_column
			
			# Load the next source sheet
			wb_src = openpyxl.load_workbook (src_path)
			sheet_src = wb_src.active
			# Get the column and row counts from next source sheet
			row_count_src = sheet_src.max_row
			col_count_src = sheet_src.max_column

			print ("")
			print ("")
			print ("File : ", src_path, "  , Qty : ", src_qty)
			print ("Row Count : ", row_count_src, "    Col Count : ", col_count_src)
			'''
			print ("Print mutually exclusive list")
			for i in range (1, row_count_out + 1):
				out_part = getQtyValPackFrom (sheet_out, i)
				print (out_part ['value'], "  :  ", out_part ['pack'])
			'''
			#print ("Sheet 2 printing now")

			# Go through all the rows of current source file and compare with output BOM file
			for loop_count_src in range (2, row_count_src + 1):
				# Get the part info from source file
				src_part = self.getQtyValPackFrom (sheet_src, loop_count_src)
				# Break flag used to detect if this part is there on the BOM Output file
				break_flag = 0
				# Take the current part just taken from source file and compare with all the
				# part in the Output BOM file 
				for loop_count_out in range (2, row_count_out + 1):
					# Row by row picking up part from Output file
					out_part = self.getQtyValPackFrom (self.sheet_out, loop_count_out)
					# Comparing of part condition
					if out_part ['value'] == src_part ['value']:
						if out_part ['pack'] == src_part ['pack']:
							# If the part is found in the output BOM file, 
							# update the qauntity and break out of the loop
							# Calculate new qauntity
							new_qty = (src_part ['qty'] * src_qty) + out_part ['qty']
							#print ("Duplicate found, new qty : ", new_qty)
							# Update the qauntity in the Cell of Output BOM file
							self.sheet_out.cell (row = loop_count_out, column = 1).value = new_qty
							# Save the Output BOM file
							self.wb_out.save (self.bom_out_path)
							# Break flag which says that part was detected in Output BOM File
							break_flag = 1
							break
				# Checks the break flag, and if the flag is 0, then that means
				# part was not detected and add the part as a new row at the end of output BOM file 
				if not break_flag:
					# This code copies the row from source file to output BOM file
					#print (src_part ['value'], "  :  ", src_part ['pack'])
					# Increment the row count of output BOM File to add the new part from source file
					row_count_out = row_count_out + 1
					# Check if qauntity value in None then don't do calculation of multiplier
					if sheet_src.cell (row = loop_count_src, column = 1).value is not None:
						# If value is not None, multiply by Multiplier and copy
						self.sheet_out.cell (row = row_count_out, column = 1).value = sheet_src.cell (row = loop_count_src, column = 1).value * src_qty
					else :
						# If value is None then just copy
						self.sheet_out.cell (row = row_count_out, column = 1).value = sheet_src.cell (row = loop_count_src, column = 1).value
					# Copy all values other then qauntity, as it is
					for col_loop_count in range (2, col_count_src + 1):
						self.sheet_out.cell (row = row_count_out, column = col_loop_count).value = sheet_src.cell (row = loop_count_src, column = col_loop_count).value
					# Save the Output BOM file
					self.wb_out.save (self.bom_out_path)
				
				self.wb_out.close ()
