from bom_combiner_routine import *
from main_win_gui_api import *
from main_win_gui_cb import *

if __name__ == "__main__":
	gui = runGUI ()
	bom_comb = BOM_Combiner (gui)
	gui_cb = MainGUICallbacks (gui, bom_comb)
	bom_comb.start ()
	gui.eventLoop ()
	bom_comb.exit ()
	sys.exit (1)
