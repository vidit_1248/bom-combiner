from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMainWindow
from PyQt5.QtGui import QPixmap
import sys
from bom_combiner_main_ui import *

class guiAPI (QMainWindow):
	def __init__ (self, app):
		super().__init__()
		# Initilize Main Window
		self.app = app
		self.MainWindow = QtWidgets.QMainWindow ()
		self.ui = Ui_MainWindow ()
		self.ui.setupUi (self.MainWindow)
		self.MainWindow.show ()
		self.perFileObjests = list ()

	def eventLoop (self):
		self.app.exec_ ()

	def addFileEntry (self, file_index):
		# Needed for updating content of ui eleents
		_translate = QtCore.QCoreApplication.translate
		# Used for position of ui elemts added with each file index
		y_offset = (file_index * 30) + 6
		# This is for new size of widget is which all the file indexes are contained
		y_offset_wid = y_offset + 38
		# Add a new dict to the for file index in our object which has all the elements of file index
		self.perFileObjests.append (dict ())
		# Updating the size of container widget to make the scroller working
		if y_offset_wid > 249:
			self.ui.scrollarea_widget.setMinimumHeight (y_offset_wid)
			self.ui.scrollarea_widget.setGeometry(QtCore.QRect(0, 0, 599, y_offset_wid))
			self.ui.file_scrollarea.setWidget (self.ui.scrollarea_widget)
		# Add frame, Add path text edit, Add browse button and Add qauntity Text edit
		self.perFileObjests [file_index]['frame'] = QtWidgets.QFrame(self.ui.scrollarea_widget)
		self.perFileObjests [file_index]['path_entry'] = QtWidgets.QPlainTextEdit(self.perFileObjests [file_index]['frame'])
		self.perFileObjests [file_index]['browse_but'] = QtWidgets.QPushButton(self.perFileObjests [file_index]['frame'])
		self.perFileObjests [file_index]['qty_entry'] = QtWidgets.QPlainTextEdit(self.perFileObjests [file_index]['frame'])
		# Frame config
		self.perFileObjests [file_index]['frame'].setGeometry(QtCore.QRect(10, y_offset, 561, 31))
		self.perFileObjests [file_index]['frame'].setFrameShape(QtWidgets.QFrame.StyledPanel)
		self.perFileObjests [file_index]['frame'].setFrameShadow(QtWidgets.QFrame.Raised)
		self.perFileObjests [file_index]['frame'].setObjectName("perfile_frame_" + str(file_index))
		# Text edit for path config
		self.perFileObjests [file_index]["path_entry"].setGeometry(QtCore.QRect(6, 4, 388, 24))
		self.perFileObjests [file_index]["path_entry"].setObjectName("path_textedit_" + str(file_index))
		#self.perFileObjests [file_index]["path_entry"].setLineWrapMode (QPlainTextEdit.NoWrap)
		# Browse button config
		self.perFileObjests [file_index]["browse_but"].setGeometry(QtCore.QRect(400, 4, 51, 23))
		self.perFileObjests [file_index]["browse_but"].setObjectName("browse_but_" + str(file_index))
		self.perFileObjests [file_index]["browse_but"].setText(_translate("MainWindow", "Browse"))
		# Text edit for qauntity config
		self.perFileObjests [file_index]["qty_entry"].setGeometry(QtCore.QRect(456, 4, 101, 24))
		self.perFileObjests [file_index]["qty_entry"].setObjectName("qty_textedit_" + str(file_index))
		self.perFileObjests [file_index]["qty_entry"].setPlainText(_translate("MainWindow", "1"))
		# Make all the things make vissible
		self.perFileObjests [file_index]['frame'].show ()

	def openFileDialogAndGetFilePath (self):
		# Opens the file dialog box and returns the path selected by user
		rcv_path = QFileDialog.getOpenFileName (filter="Excel File (*.xlsx)")
		return rcv_path

	def updateFilePathTesxtEdit (self, file_index, path):
		self.perFileObjests [file_index]["path_entry"].setPlainText (path)

	def outputFileSavePathAndUpdateGUI (self):
		rcv_path = QFileDialog.getSaveFileName (filter="Excel File (*.xlsx)")
		self.ui.output_path_textedit.setPlainText (rcv_path [0])

	def getAllPathAndQty (self):
		ret_list = list ()
		for file_info in self.perFileObjests:
			ret_list.append ([file_info ["path_entry"].toPlainText(), int(file_info ["qty_entry"].toPlainText())])
		return ret_list

	def getOutputPath (self):
		return self.ui.output_path_textedit.toPlainText ()
	
	def setMergeButStateTo (self, but_state):
		if but_state == 0:
			self.ui.mergebom_but.setEnabled (False)
		else:
			self.ui.mergebom_but.setEnabled (True)

	def disableAndchangeNameOfMergBut (self, name):
		# Needed for updating content of ui eleents
		_translate = QtCore.QCoreApplication.translate
		self.ui.mergebom_but.setText(_translate("MainWindow", name))
		self.setMergeButStateTo (0)

	def enableAndchangeNameOfMergBut (self, name):
		# Needed for updating content of ui eleents
		_translate = QtCore.QCoreApplication.translate
		self.ui.mergebom_but.setText(_translate("MainWindow", name))
		self.setMergeButStateTo (1)

def runGUI ():
	app = QtWidgets.QApplication (sys.argv)
	gui = guiAPI (app)
	return gui