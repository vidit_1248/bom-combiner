from PyQt5.QtWidgets import QMainWindow, QMessageBox
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QPixmap
import queue as Queue

class Result_Box_UI (QtCore.QThread):
	def __init__ (self):
		QtCore.QThread.__init__ (self)
		self.thread_active = False
		self.box_cmd_q = Queue.Queue()

	def run (self):
		self.thread_active = True
		while  self.thread_active:
			if not self.box_cmd_q.empty ():
				if self.combine_q.get () == 1:
					self.showDonePopUp ()
				if self.combine_q.get () == 0:
					self.showErrorPopUp ()

	def exit (self):
		self.thread_active = False
	
	def showDonePopUp (self):
		""" Shows a success message box """
		msg_box = QMessageBox ()
		msg_box.setWindowTitle ("Update")
		msg_box.setText ("Success in merging file!")
		#msg_box.setIconPixmap(QPixmap("./ui_graphic/done.png"))
		temp = msg_box.exec_ ()

	def showErrorPopUp (self):
		""" Shows a success message box """
		msg_box = QMessageBox ()
		msg_box.setWindowTitle ("Update")
		msg_box.setText ("Failed to merge file!")
		#msg_box.setIconPixmap(QPixmap("./ui_graphic/done.png"))
		temp = msg_box.exec_ ()