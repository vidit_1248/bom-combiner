# BOM Combiner
This code is written to save electronics engineers from their misery of combining BOM 
They fall in this misery when they have to place order for components for multiple awesome PCBs they just made 
This code takes in the paths of excel sheets of multiple BOM needed to be combined, it copies the first BOM in list as a new file and then adds data of next sheets one by one.
It also enables you to give a quantity for each sheet which enable you order different quantity of PCBs for each PCBs
You can theoretically combine any number of BOMs

## How to get it
### Windows
The exe file that can run in windows can be found in dist folder of the repository. It has been tested for Windows 10

### Run the code directly (OS Agnostic)
It is assumed you are a developer
1. Clone the repository
2. Install python3 and following library using pip: pyqt5, openpyxl
3. Run main_app.py

## How to use
### Windows
1. Start the given bom_combiner.exe 
  <img src = "readme_images/plain_start.PNG">

2. Add the files you want combine and put quantity for each file 
  <img src = "readme_images/filled_up.PNG"> 
  <img src = "readme_images/add_file.PNG">

3. Browse the output location of result BOM 
  <img src = "readme_images/browse_output.PNG">

4. Click the "Merge all BOM" button
  <img src = "readme_images/click_merge.PNG">

5. It will process the BOMs
  <img src = "readme_images/merge_processing.PNG">

6. After finishing button will go back to normal
  <img src = "readme_images/filled_up.PNG">

7. Result can be seen in the command line window running along with it
  <img src = "readme_images/succes_msg.PNG">

## Known issues (and things that will break it)
1. If the path is long, it becomes vertical scrollable on place of horizontal scroll
2. If the paths or quantity are left empty the software might just crash, get into error which can be seen in the command line window along with it
3. It case sensitive in combining BOM which means 100k and 100K are seen as different value
4. It only works with defined column locations. Sample files are attached
5. Right now it is just footprint and value based

## Feature in Pipeline
1. Error popups if path entry is empty or qty entry is empty
2. GUI Should have function to determine which column is what field
3. There has to e two modes of merging, MPIN based and Value + Footprint based
4. Make new column for each board and add their references in front of the part
5. Store output BOM file with current data and time, and if file already exist, make (n) file, but don't replace the file

## Development related tasks in Pipeline
1. Create more separation in GUI elements related code, callbacks of GUI and functional code
2. Improve the documentation and commenting
3. Create release for multiple OS

## How sheet merging works
1. Take in multiple file paths with qty in single argument and output file path as a second argument
2. If only one file is given just copy that file to output file and multiply all quantity with given file quantity
3. In case of multiple file, it copies the first file as output file and multiply all quantity with given file quantity
4. Then it open next file, takes up first row and compare it's value and footprint with all the rows of output BOM
5. If it finds a row with same value and footprint, it take quantity of row of current file, multiply it with given file quantity and add to the quantity of the target row in the output file
6. If it doesn't find a row with same value and footprint, just append the row at the end of file
7. Does this with all rows
8. And same thing with all files one by one

## Useful commands
Command to generate python code from ui file of designer

`pyuic5 -x bom_combiner_main.ui -o bom_combiner_main_ui.py`

Generate the exe file

`pip install pyinstaller`

`pyinstaller -F .\main_app.py`

## How to contribute
Not sure yet will be updated soon

## Issues, Troubleshooting and suggestion
Feel free to raise these things in issue tracker
